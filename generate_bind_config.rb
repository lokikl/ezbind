#!/usr/bin/env ruby

require 'yaml'
require 'erb'

if ENV['CONFIG'].to_s == ''
  raise "ENV['CONFIG'] is expected but missing"
end
config = YAML.load(ENV['CONFIG'])

config['zones'].each { |domain, records|
  zone_file_content = <<~TEMPLATE
    $TTL 1h
    @ IN SOA ns1.#{domain} contact.#{domain}. (
                #{Time.now.to_i}      ; Serial
                1h              ; Refresh
                15m             ; Retry
                1d              ; Expire
                1h );           ; Negative Cache TTL
    #{domain}.      1800    IN    NS        ns1.#{domain}.
    ns1             1800    IN    A         172.28.28.28
    TEMPLATE

  records['pointer_records'].each { |subdomain, address|
    zone_file_content << %Q[#{subdomain}  1800  IN  A  #{address}\n]
  }

  File.write("/etc/bind/#{domain}.zone", zone_file_content)
}


zones_str = config['zones'].keys.map { |domain|
  [
    %Q[zone "#{domain}" IN {],
    %Q[    type master;],
    %Q[    file "/etc/bind/#{domain}.zone";],
    %Q[};]
  ]
}.flatten.join("\n")

config['forwarders'] ||= ['1.1.1.1', '1.0.0.1']
forwarders_str = config['forwarders'].map { |forwarder|
  %Q[        #{forwarder};]
}.join("\n")

named_conf = File.read('/etc/bind/named.conf')

named_conf.sub!('#ZONES_HERE#', zones_str)
named_conf.sub!('#FORWARDERS_HERE#', forwarders_str)

File.write('/etc/bind/named.conf', named_conf)
